"""
BPH BankConnect API wrapper.
Minimal version for account operations history for sake of simplicity.
"""

__author__ = 'ra2er, <sylwester.kulpa@gmail.com>'

from .client import APIClient

__all__ = ['APIClient']